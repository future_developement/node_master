/****************************
 SEED DATA
 ****************************/
const _ = require("lodash");
const EmailTemplate = require('../modules/EmailTemplate/Schema').EmailTemplate;
const Admin = require('../modules/Admin/Schema').Admin;
const Model = require("../modules/Base/Model");
const CommonService = require("./Common");

class Seed {

    constructor() { }

    async seedData() {
        try {
            this.addEmailTemplate();
            this.addAdmin();
        } catch (error) {
            console.log('error', error);
        }
    }
    async addEmailTemplate() {
        try {
            let registerMail = {
                'emailKey': "signup_mail",
                'subject': "Welcome Message",
                'emailContent': "<p><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Congratulations {{{fullName}}} for signing up with App. Your experience with us is the highest priority. We welcome you to get to know our company and its features. </span><br><br><a href=\"{{{verificationLink}}}\" target=\"_self\"><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Click link to verify your account</span></a><br><br></p>"
            };
            let isKeyExist = await EmailTemplate.findOne({ emailKey: registerMail['emailKey'] }).select({ "_id": 1 });
            if (!isKeyExist) {
                await new Model(EmailTemplate).store(registerMail);
            }
            let forgotPasswordMail = {
                'emailKey': "forgot_password_mail",
                'subject': "Reset password",
                'emailContent': "<p><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Dear {{{fullName}}}, click below link to reset password. </span><br><br><a href=\"{{{resetPasswordLink}}}\" target=\"_self\"><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Click link to reset password</span></a><br><br></p>"
            };
            isKeyExist = await EmailTemplate.findOne({ emailKey: forgotPasswordMail['emailKey'] }).select({ "_id": 1 });
            if (!isKeyExist) {
                await new Model(EmailTemplate).store(forgotPasswordMail);
            }
            let adminUserInviteMail = {
                'emailKey': "admin_invite_mail",
                'subject': "Admin Welcome Message",
                'emailContent': "<p><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Dear {{{fullName}}} . You were added as {{{role}}}. </span><br><br><a href=\"{{{verificationLink}}}\" target=\"_self\"><span style=\"color: rgb(0,0,0);font-size: 13px;font-family: Arial;\">Click link to set password for your account</span></a><br><br></p>"
            };
            isKeyExist = await EmailTemplate.findOne({ emailKey: adminUserInviteMail['emailKey'] }).select({ "_id": 1 });
            if (!isKeyExist) {
                await new Model(EmailTemplate).store(adminUserInviteMail);
            }
            return;
        } catch (error) {
            console.log('error', error);
            return;
        }
    }
    async addAdmin() {
        try {
            let admin = await Admin.findOne({ "emailId": "seed_admin@grr.la" });
            if (!admin) {
                let password = "Test1234";
                password = await (new CommonService()).ecryptPassword({ password: password });
                let data = {
                    "firstname": "Admin",
                    "lastname": "Admin",
                    "mobile": "+91-0000000000",
                    "emailId": "seed_admin@grr.la",
                    "password": password,
                    // "role": "admin",
                    "status": true,
                    "emailVerificationStatus": true,
                };
                await new Model(Admin).store(data);
            }
            return;
        } catch (error) {
            console.log('error', error);
            return;
        }
    }
}

module.exports = Seed;