/****************************
 Configuration
 ****************************/
module.exports = {
    db: 'mongodb://localhost/test_db',
    mongoDBOptions: {
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        keepAlive: 1,
        connectTimeoutMS: 30010,
        useNewUrlParser: true,
        useFindAndModify: false,
        native_parser: true,
        poolSize: 5,
       // user: '{username}',
        //pass: '{password}'
    },

    sessionSecret: 'indNIC2305',
    securityToken: 'indNIC2305',
    securityRefreshToken: 'indNIC2305refresh',

    baseApiUrl: '/api',
    host: "10.2.2.77",
    serverPort: '3001',
    tokenExpiry: 361440, // Note: in seconds! (1 day)

    rootUrl: 'http://10.2.2.77:3001/api',
    frontUrl: 'http://10.2.2.77:3001',
    frontUrlAngular: 'http://ng7adminseed.node.indianic.com/#/public',

    defaultEmailId: 'meanstack2017@gmail.com',
    apiUrl: 'http://10.2.2.77:3001/api',
    
    perPage: 20,
    adPerPage: 4,
    
    s3upload: false,
    localImagePath: "/public/upload/images/",
    s3ImagePath: "",

    dontAllowPreviouslyUsedPassword: true,
    storePreviouslyUsedPasswords: true,

    forceToUpdatePassword: true,
    updatePasswordPeriod: 4, // In months

    allowedFailAttemptsOfLogin: 5,
    isBlockAfterFailedAttempt: true,
    timeDurationOfBlockingAfterWrongAttempts: 15, // In minutes

    tokenExpirationTime: 540, // minutes
    forgotTokenExpireTime: 60, // minutes
    verificationTokenExpireTime: 60, // minutes

    extendTokenTime: true,
    useRefreshToken: true,

    isHTTPAuthForSwagger: false,
    HTTPAuthUser: "root",
    HTTPAuthPassword: "root"
};
